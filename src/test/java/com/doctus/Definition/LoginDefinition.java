package com.doctus.Definition;

import com.doctus.Steps.LoginSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class LoginDefinition {
	@Steps
	LoginSteps loginSteps;
	
	@Given("^Estando en pagina familia$")
	public void estando_en_pagina_familia() throws Exception {
		loginSteps.AbrirAplicativo();
	}


	@When("^Digito \"([^\"]*)\" y \"([^\"]*)\"$")
	public void digito_y(String usuario, String contrasena) throws Exception {
		loginSteps.DigitaUsuarioyPassword(usuario, contrasena);
	}

	@Then("^Verifico mensaje \"([^\"]*)\" y tipo \"([^\"]*)\"$")
	public void verifico_mensaje_y_tipo(String mensaje, String tipo) throws Exception {
		loginSteps.VerificaMensaje(tipo, mensaje);
	}
}
