package com.doctus.Steps;

import com.doctus.PageObjets.LoginPage;

import net.thucydides.core.annotations.Step;

public class LoginSteps {
	
	LoginPage loginPage;
	
	@Step
	public void AbrirAplicativo() {
		loginPage.open();
	}
	
	@Step
	public void DigitaUsuarioyPassword(String usuario,String password) {
		loginPage.txtUsuario(usuario);
		loginPage.txtPassword(password);
		loginPage.btnIngresar();
	}
	
	@Step
	public void VerificaMensaje(String tipo,String mensaje) {
		loginPage.VerificaMensaje(tipo, mensaje);
	}

}
