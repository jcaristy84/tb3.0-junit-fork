package com.doctus.PageObjets;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://familiatiendaideal-qa.azurewebsites.net")
public class LoginPage extends PageObject {
	
	@FindBy(xpath = "//input[@id='email']")  
	public WebElementFacade txtUsuario;
	
	@FindBy(xpath = "//input[@id='password']")  
	public WebElementFacade txtPassword;	

	@FindBy(xpath = "//button[@class='blue-btn']")  
	public WebElementFacade btnIngresar;

	@FindBy(xpath = "//div[@class='alert alert-danger']")  
	public WebElementFacade lblmensajeerror;	
	
	@FindBy(xpath = "//*[contains(text(),'Transforma')]")  
	public WebElementFacade lblinicio;	
	
	@FindBy(xpath = "//div[@class='user-icon']")  
	public WebElementFacade btnUsuario;	
	
	@FindBy(xpath = "//a[@class='user-stores__close']")  
	public WebElementFacade btnLogout;	
		
		
	public void txtUsuario(String usuario) {
		txtUsuario.type(usuario);
	}
	public void txtPassword(String password) {
		txtPassword.type(password);
	}	
	public void btnIngresar() {
		waitFor(1).seconds();
		Serenity.takeScreenshot();
		btnIngresar.click();		
	}
	public void VerificaMensaje(String tipo,String mensaje) {
		waitFor(1).seconds();
		Serenity.takeScreenshot();
		switch (tipo) {
		case	"feliz": assertThat(lblinicio.getText().replaceAll("\n", ""), containsString(mensaje));
		waitFor(500).milliseconds();
		btnUsuario.click();
		waitFor(500).milliseconds();
		btnLogout.click();
		waitFor(500).milliseconds();
		break;
		case	"no feliz":	assertThat(lblmensajeerror.getText().replaceAll("\n", ""), containsString(mensaje));
		break;
		}
	}
}
