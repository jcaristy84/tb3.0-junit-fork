
Feature: Pruebas Devops
  Se verifica logueo caso feliz y no feliz

  Scenario Outline: Logue familia
    Given Estando en pagina familia
    When Digito "<usuario>" y "<contrasena>"
    Then Verifico mensaje "<mensaje>" y tipo "<tipo>"
 	Examples:
 	|usuario          |contrasena|tipo    |mensaje                                                                   |
 	|vendedor@test.com|12345678  |feliz   |Transforma tus tiendasy lleva felicidad junto con                         |
 	|vendedor@test.com|1234567   |no feliz|Por favor verifique que su correo electrónico y contraseña sean correctos |
 	