Feature: Pruebas Devops Doctur
  Se verifica logueo caso feliz y no feliz

  Scenario Outline: Logueo Doctus
    Given Estando en pagina doctus
    When Ir a nuevo proyecto
    And Digito datos de proyecto "<nombre>" y "<descripcionx>"
    Then Verifico mensaje "<mensaje>" tipo "<tipo>"
 	Examples:
  |usuario          |contrasena|tipo    |mensaje                                                                   |
 	|vendedor@test.com|12345678  |feliz   |Transforma tus tiendasy lleva felicidad junto con                         |
 	|vendedor@test.com|1234567   |no feliz|Por favor verifique que su correo electrónico y contraseña sean correctos |
